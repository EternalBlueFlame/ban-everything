package ebf.baneverything;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

class BannedPlayer {
    UUID account =null;
    String name=null;
    long bantime=0L;
    List<String> ipAddress=new ArrayList<>();
    String macAddress=null;
    List<String> bypassRoles=null;

    public String writeString() {
        BanEverything.builder = new StringBuilder();
        BanEverything.builder.append("Name[");
        BanEverything.builder.append(name);
        BanEverything.builder.append("] Ban Hours [");
        BanEverything.builder.append(bantime);
        BanEverything.builder.append("] Mojang UUID [");
        BanEverything.builder.append(account.toString());
        BanEverything.builder.append("] MAC Address [");
        BanEverything.builder.append(macAddress);
        BanEverything.builder.append("] IP Address [");
        if (ipAddress != null) {
            for (String ip : ipAddress) {
                BanEverything.builder.append(ip);
                BanEverything.builder.append(",");
            }
        }
        BanEverything.builder.append("] Groups [");
        if (bypassRoles != null) {
            for (String ip : bypassRoles) {
                BanEverything.builder.append(ip);
                BanEverything.builder.append(",");
            }
        }
        BanEverything.builder.append("]}");
        return BanEverything.builder.toString();
    }


    public BannedPlayer readString(String data){
        String currentParse= data.substring(data.indexOf("{[")+1);
        name=currentParse.substring(0,currentParse.indexOf("] Ban Hours ["));
        currentParse=currentParse.substring(currentParse.indexOf("[")+1);
        bantime = Long.parseLong(currentParse.substring(0,currentParse.indexOf("] Mojang UUID [")));
        currentParse=currentParse.substring(currentParse.indexOf("[")+1);

        account = UUID.fromString(currentParse.substring(0,currentParse.indexOf("] MAC Address [")));
        currentParse=currentParse.substring(currentParse.indexOf("[")+1);

        macAddress = currentParse.substring(0,currentParse.indexOf("] IP Address ["));
        currentParse=currentParse.substring(currentParse.indexOf("[")+1);

        String[] split = currentParse.substring(0, currentParse.indexOf("] Groups [")).split(",");
        ipAddress.addAll(Arrays.asList(split));
        currentParse=currentParse.substring(currentParse.indexOf("[")+1);

        split = currentParse.substring(0,currentParse.indexOf("]}")).split(",");
        bypassRoles.addAll(Arrays.asList(split));

        return this;
    }

}

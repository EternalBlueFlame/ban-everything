package ebf.baneverything;

import net.minecraft.command.CommandBase;
import net.minecraft.command.ICommandSender;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;

import java.util.ArrayList;
import java.util.List;

class BannedItem {
    String unlocalizedName=null;
    int id,meta;
    NBTTagCompound nbtData =null;
    List<String> bypassRoles=new ArrayList<>();

    public BannedItem genItem(ItemStack itm, boolean saveNBT, String[] data){
        if(itm==null){
            return null;
        }
        unlocalizedName=itm.getUnlocalizedName();
        id= Item.getIdFromItem(itm.getItem());
        meta=itm.getItemDamage();

        if(saveNBT){
            nbtData=itm.stackTagCompound;
        }

        for (int i=1; i<data.length;i++){
            if(i!=1 || !data[i].equals("NBT")){
                bypassRoles.add(data[i]);
            }

        }
        return this;

    }

    public String write(){
        BanEverything.builder = new StringBuilder();

        BanEverything.builder.append(unlocalizedName);
        BanEverything.builder.append(",");
        BanEverything.builder.append(id);
        BanEverything.builder.append(",");
        BanEverything.builder.append(meta);
        BanEverything.builder.append(",");
        if(nbtData==null){
            BanEverything.builder.append("null");
        } else {
            //todo: NBT Saving
            BanEverything.builder.append("null");
        }

        return BanEverything.builder.toString();
    }

    public static BannedItem read(String s){
        String[] values= s.split(",");
        BannedItem itm = new BannedItem();
        itm.unlocalizedName=values[0];
        itm.id = Integer.parseInt(values[1]);
        itm.meta= Integer.parseInt(values[2]);
        if(values[3].equals("null")){
            itm.nbtData=null;
        } else {
            //todo: NBT parsing
            itm.nbtData=null;
        }

        return itm;
    }


    public static CommandBase banItem = new CommandBase() {
        @Override
        public String getCommandName() {
            return "banItem";
        }

        @Override
        public String getCommandUsage(ICommandSender p_71518_1_) {
            return "\"/banItem NBT GROUPS\" only include \"NBT\" if you want the ban specific to that item with that NBT, this is a rare case";
        }

        @Override
        public void processCommand(ICommandSender p_71515_1_, String[] p_71515_2_) {
            BannedItem itm = new BannedItem().genItem(getCommandSenderAsPlayer(p_71515_1_).inventory.getCurrentItem(),
                    p_71515_2_[1].equals("NBT"), p_71515_2_);
            if(itm!=null) {
                BanEverything.bannedItems.add(itm);
            }

        }
    };


}

package ebf.baneverything;


import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.event.FMLServerStartingEvent;
import net.minecraft.command.CommandBase;
import net.minecraft.command.ICommandSender;
import net.minecraftforge.common.config.Configuration;

import java.util.ArrayList;
import java.util.List;

import static sun.security.x509.ReasonFlags.UNUSED;


@Mod(modid = "baneverything", version = "pre-release", name = "Ban Everything")
public class BanEverything {

    Configuration cfg;
    protected static StringBuilder builder;

    protected static List<BannedPlayer> bannedPlayers = new ArrayList<>();

    protected static List<BannedItem> bannedItems = new ArrayList<>();

    //log when one of the ban checks is triggered, and by who
    protected static List<String> banInteractionLog = new ArrayList<>();

    @Mod.EventHandler
    @SuppressWarnings(UNUSED)
    public void preInit(FMLPreInitializationEvent event) {
        cfg = new Configuration(event.getSuggestedConfigurationFile());
        cfg.load();

        String[] items = cfg.getStringList("itemData", "ItemData", new String[]{""},
                "This is the list of banned items and their related known information");

        String[] players = cfg.getStringList("playerData", "PlayerData", new String[]{""},
                "This is the list of banned players and their related known information");


        cfg.save();

    }


    @Mod.EventHandler
    @SuppressWarnings(UNUSED)
    public void postInit(FMLPostInitializationEvent event) {



    }

    public void cfgUpdate(){
        List<String> output = new ArrayList<>();
        for(BannedItem itm : bannedItems){
            output.add(itm.write());
        }
        cfg= new Configuration();
        cfg.getStringList("itemData", "ItemData", output.toArray(new String[0]),
                "This is the list of banned items and their related known information");


        cfg.save();
    }


    //todo: pickup event
    //todo: drop event
    //todo: inventory pickup event
    //todo: item use event
    //todo: block place event
    //todo: block break event
    //todo: interact event









    @Mod.EventHandler
    @SuppressWarnings(UNUSED)
    public void initCommands(FMLServerStartingEvent event) {

        event.registerServerCommand(BannedItem.banItem);

    }



}
















